// Shared code needed by the code of all three pages.
// Prefix to use for Local Storage.  You may change this.

var APP_PREFIX = " ";

// Array of saved Region objects.
var savedRegions = [];

//Array of marked regions
var markedRegion = [];

//The address to the region list
var REGION_LIST_KEY= ".RegionList"
    
class Region
{
    constructor(location, time, regionName)
    {
        this._name= regionName
        this._location= []; //Adds the longitude and latitude
        this._time= time;
    }
    
    // getter methods
    get regionArea(){
        return this.calculateRegionArea();
    }
    // method- any calculations neccessary 
    
    calculateRegionArea(){
        
        let geod = GeographicLib.Geodesic.WGS84;
		
		let regionCreated = geod.Polygon(false);
		for (let i = 0; i < this._arrCorner.length; i++) {
			let longitudelat = this._location[i];
			regionCreated.AddPoint(longitudelat[1],longitudelat[0]);//lat lon
		}
		
		let retn = regionCreated.Compute(true,true);
		
		return retn.area.toFixed(1) + " m^2";
	}
    get time(){
        return this._time;
    }
    
    get location(){
        
    return this._location;
        
    }
    set location(newLocation){
        
        this._location= newLocation;
    }
    set time(newTime){
    
    this._time= newTime;
    }
    BoundaryFencePosts(){
        var regionCreated = geod.Inverse(-41.32, 174.81, 40.96, -5.50);
		console.log("The distance is " + r.s12.toFixed(3) + "m");
		var a = geod.Inverse(-41.32, 174.81, 40.96, -5.50, GeographicLib.Geodesic.AZIMUTH);
		console.log("The azimuths is " + a.azi1.toFixed(3) + "," + a.azi2.toFixed(3));
    }
    toString(){
        return this._cornerLocation.toString + "," + this._time.toString();
    }
}


class RegionList 
{
    construction (regions,countOfRegions)
    {
        this._listRegions = regions;
        this._countOfRegions =countOfRegions;
       
    }
   
    getRegion(nUmber) {
         
        if(nUmber >= 0 && nUmber < this._regions.length){
			return this._listRegions[nUmber];
        }
    }  
    addRegion(region){
        this._listRegions.push(region);
        this._countOfRegions++;
    }
    getRegionNumber(){
        return this._countOfRegions;
    }
    getRegionIndex(id) {
        return this._listRegions.find(regionCreated => regionCreated._name == id);
    }


    removeRegion(nUmber) {
        this._listRegions.slice(a => a._name== id, 1);
        if(nUmber >= 0 && nUmber < this._listRegions.length){
			
			let regionCreated =  this._listRegions[nUmber];
			if(nUmber != this._listRegions.length-1){
				this._listRegions = this._listRegions.slice(0,nUmber).concat(this._listRegions.slice(nUmber+1));
			}
			else{
				this._listRegions = this._listRegions.slice(0,nUmber);
			}
			this._countOfRegions--;

			return regionCreated;
		}
	}
    popRegion(){
		if(this._countOfRegions >= 1){
			this.removeRegion(this._countOfRegions-1);
		}
	}

	toString(){
		return this._listRegions.toString();
	}

    getTotRegions(){
        return this._listRegions;
    }
    
   
    numberOfRegionsUpdate(){
        this._countOfRegions = this._listRegions.length;
    }
    getNumberOfRegions(){
        return this._listRegions.length;
    }
    
}

// Creating new regions passing in the time and location
function reorganiseRegionList(){
    regL= JSON.parse(localStorage.getItem("APP_PREFIX"));
    if (regL !=null){
       regList = new RegionList([],0);
        for (let i = 0; i< regL._countOfRegions; i++){
            let regn= regL._listRegions[i];
            if (regn !=null){
                regnTemp = new Region (regn._time)
                regnTemp.location= regn._location;
                regList.addRegion(regnTemp);
            }
                
        }
        return regList
    }
}

let regionList = new RegionList(); //creating new customised region list



function eraseRegion(region) {
  var index = region.parentNode.parentNode.regionIndex;
  document.getElementById("regionName").eraseRegion(i);
}
function showRegionList (listOfRegions, markerShow){
    removeRegnByID('rgnLstID');
    MyRegions=[];
    let nUmber= listOfRegions.getNumberOfRegions();
    for (let i = 0; i<nUmber; i++){
        let regn= regionLST.getRegion(i);
        if (regn!= null){
            regnTemp = new Region (regn._time);
            regnTemp.location= regn._location;
            
// slice/ remove location selected instead of deleting all
        let reg = regnTemp.location.slice();
            cornerOne= reg[0];
            reg.push(cornerOne);
            coordinates = []
            for (j = 0; j<reg.length; j++){
                longitudeLat= reg [j]
                coordinates.push(longitudeLat);
                }
             var dic = {
                'type': 'Feature',
                'geometry': {
                    'type': 'Polygon',
                    'coordinates': [coordinates]
                }
	        };
	        MyRegions.push(dic);
        }
    }
    
    
	map.addSource('rgnLstID', {
        'type': 'geojson',
        'data': {
            'type': 'FeatureCollection',
            'features': MyRegions
        }
	});

    map.addLayer({
        'id': 'rgnLstID',
        'type': 'fill',
        'source': 'rgnLstID',
        'layout': {},
        'paint': {
            'fill-color': '#000',
            'fill-opacity': 0.8
        }
    });
}


// Code for the Record Region page.
"use strict";

// This should be your own API key
mapboxgl.accessToken = 'pk.eyJ1IjoidmljdG9yeWtoZXZhbGkiLCJhIjoiY2tqc2VoZ3B5Nm05aDJ4cDk0MTgyajB2YSJ9.2e5wKrblFQKereTlz5jpKQ';
var map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/mapbox/streets-v11',//stylesheet location 
center: [153.02809,-27.46794], //centre location
zoom: 15
});
    
let regionCreated = new Region(new Date());

let regL = JSON.parse(localStorage.getItem("APP_PREFIX"));

let regList = null;
if(regL == null){
	regList = new RegionList([],0);
}
else{
	regList = new RegionList(regL._regions,regL._countOfRegions);
}

function saveRegionInstance(currentRegion) {

	if(typeof(Storage) != "undefined"){
		
		let num = regList.getRegionNumber();
		if(num >= 1){
			if(JSON.stringify(regList.getRegion(num-1)) === JSON.stringify(currentRegion)){
				regList.popRegion();
			}
		}
		
		regList.addRegion(currentRegion);

		localStorage.setItem("APP_PREFIX",JSON.stringify(regList));
		
	}

}


//Get user's current location found in:- Ref:https://stackoverflow.com/questions/54405968/how-to-get-the-current-location-of-the-user
function goToCurrentLocation() {
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(position => {
            map.flyTo({
                center: [position.coords.longitude, position.coords.latitude],
                essential: true
            })

            let popup = new mapboxgl.Popup({ closeOnClick: false })
                .setLngLat([position.coords.longitude, position.coords.latitude])
                .setText("Your current location")
                .addTo(map);

            new mapboxgl.Marker()
                .setLngLat([position.coords.longitude, position.coords.latitude])
                .setPopup(popup)
                .addTo(map);
        });
    }
    //if geolocation IS NOT available, or user doesn't allow for their current location to be accessed
    else {
        alert("To access this feature, Location is required")
    }
}


let cornerLocations = [];

//Creating regions, adding them and resetting them
let geojson = {
    type: "FeatureCollection",
    features: []
};

// Used to draw a line between points
let linestring = {
    type: "Feature",
    geometry: {
        type: "LineString",
        coordinates: []
    }
};

map.on("load", function () {
    map.addSource("geojson", {
        type: "geojson",
        data: geojson
    });

    map.addLayer({
        id: "measure-points",
        type: "circle",
        source: "geojson",
        paint: {
            "circle-radius": 5,
            "circle-color": "#005"
        },
        filter: ["in", "$type", "Point"]
    });

    map.addLayer({
        id: "measure-lines",
        type: "line",
        source: "geojson",
        layout: {
            "line-cap": "round",
            "line-join": "round"
        },
        paint: {
            "line-color": "#000",
            "line-width": 2.5
        },
        filter: ["in", "$type", "LineString"]
    });

    map.on("click", function (e) {
        

        if (geojson.features.length > 1) {
            geojson.features.pop();
        }

        var point = {
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: [e.lngLat.lng, e.lngLat.lat]
            },
            properties: {
                id: String(new Date().getTime())
            }
        };

        if (geojson.features.length == 0) {
            //Initial point
            geojson.features.push(point);
            //Destination point
            geojson.features.push(point);
        } else {
            geojson.features.splice(geojson.features.length - 1, 0, point);
        }

        linestring.geometry.coordinates = geojson.features.map(function (point) {
            return point.geometry.coordinates;
        });

        //show path
        geojson.features.push(linestring);

        cornerLocations= linestring.geometry.coordinates;

        

        map.getSource("geojson").setData(geojson);
    });
});


document.getElementById("buttonResetRegion").addEventListener("click", function (e) {

    geojson.features = [];

    cornerLocations = [];

    // console.log(geojson);

    map.getSource("geojson").setData(geojson);

});

document.getElementById("buttonAddRegion").addEventListener("click", function (e) {
    if (cornerLocations.length < 4) {
        alert("Please don't select less than 3 corners.");
    } else {
        let regionName = prompt("Please give a desired name for this region: ");

        if (regionName != null && regionName.trim() != "") {
            if (!saveRegionInstance(regionList).includes(regionName.trim())) {

                let region = new Region(regionName, cornerLocations);
                regionList.addRegion(region);
                regionList.numberOfRegionsUpdate();

                localStorage.setItem(REGION_LIST_KEY, JSON.stringify(regionList));

                window.location = "index.html";

            } else {
                alert("Region name already exists. Please enter a different one.");
            }

        } else {
            alert("Region name cannot be empty.")
        }
    }



});

document.getElementById("buttonCurrentLocation").addEventListener("click", function (e) {

    goToCurrentLocation();

});

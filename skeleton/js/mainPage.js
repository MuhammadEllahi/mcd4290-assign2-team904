// Code for the main app page (Regions List).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.
function viewRegions (regionIndex)
{
    //save the region chosen to the local storage so that it is accessible in the view region page
    localStorage.setItem(APP_PREFIX + "pickedRegion", regionIndex);
    // loading the view region page
    location.href = "viewRegion.html";
}


/*if (localStorage.getItem('regions') != undefined) {
    console.log("Show data from local storage")
    showRegions(getCorners());
}

//display data on table
function showRegions(data) {
    var table = document.getElementById("regions_table");

    for (let i = 0; i < data.length; i++) {
        var row = table.insertRow(i + 1);
        row.insertCell(0).innerHTML = data[i].name;
        row.insertCell(1).innerHTML = data[i].corner1.name;
        row.insertCell(2).innerHTML = data[i].corner2.name;
        row.insertCell(3).innerHTML = data[i].corner3.name;
        row.insertCell(4).innerHTML = data[i].distance;
        
    }
}


//get corners from local storage
function getRegions(regionIndex) {
    let data = JSON.parse(localStorage.getItem('regions'));
    return data;

}*/


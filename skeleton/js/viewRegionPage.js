// Code for the View Region page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

let regionIndex = localStorage.getItem(APP_PREFIX + "-selectedRegion"); 
if (regionIndex !== null)
{
    // If a region index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the region being displayed.
    var regionNames = [ "Region A", "Region B" ];
    document.getElementById("headerBarTitle").textContent = regionNames[regionIndex];
}

// This should be your own API key
mapboxgl.accessToken = 'pk.eyJ1IjoidmljdG9yeWtoZXZhbGkiLCJhIjoiY2tqc2VoZ3B5Nm05aDJ4cDk0MTgyajB2YSJ9.2e5wKrblFQKereTlz5jpKQ';
let map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/mapbox/streets-v11',//stylesheet location 
center: [153.02809,-27.46794], //centre location
zoom: 15
});

map.on('load', function () {

	regL = JSON.parse(localStorage.getItem("APP_PREFIX"));
	if(regL != null){
		regList = new RegionList(regL._listRegions,regL._countOfRegions);
		
		let regn = regList.getRegion(Number(regionIndex));  
		if(regn != null){
			regnTemp = new Region(rgn._time);
			regnTemp.arrCorner = rgn._arrCorner;
			document.getElementById("areaBarTitle").textContent = regnTemp.calculateArea();
			////
			rgnTemp.boundaryFencePosts();
			////
			showPolygon(regnTemp,false);
		}
	}

	map.on('click', function(e) {    	
    	displayRemoveRegionMessage('remove region',1000);
	});

});

function deleteRegion() {

	regList = rebulidRegionList();
	
	if(regList != null){
		if(typeof(Storage) != "undefined"){
			let num = regList.getRegionCount();
			if(num >= 1){	
				regList.removeRegion(Number(regionIndex));
			}
			localStorage.setItem("APP_PREFIX",JSON.stringify(regList));
		}
	}
}
